clc;
r_gimg1 = imresize(rgb2gray(imread('eunha.png')),0.5);
r_gimg2 = imresize(rgb2gray(imread('sowon.png')),0.5);

c_gimg1 = imcrop(r_gimg1,[150 40 350 400]);
a_gimg1 = imadd(c_gimg1,20);

he_gimg1 = histeq(a_gimg1);

h1 = 1/9 * ones(3,3);
b1 = conv2(he_gimg1,h1,'same');
bw_img1 = im2bw(uint8(b1));

se1 = strel('disk',9,0);
close_bw1 = imclose(bw_img1, se1);

[x,y,level] = size(bw_img1);
for i = 2 : x-1
    for j = 2 : y-1
        if (close_bw1(i-1,j) == close_bw1(i+1,j)) || (close_bw1(i,j+1) == close_bw1(i,j-1))
            tepi_img1(i,j) = 0;
        else
            tepi_img1(i,j) = 1;
        end
    end
end
         

%-------------------------------------------------

img2 = imread('sowon.png');
gimg2 = rgb2gray(img2);
r_gimg2 = imresize(gimg2,0.5);
c_gimg2 = imcrop(r_gimg2,[150 40 350 400]);
a_gimg2 = imsubtract(c_gimg2,60);
he_gimg2 = histeq(a_gimg2);

h2 = 1/9 * ones(3,3);
b2 = conv2(he_gimg2,h2,'same');
bw_img2 = im2bw(uint8(b2));

se2 = strel('disk',9,0);
close_bw2 = imclose(bw_img2, se2);

[x,y,level1] = size(bw_img2);
for i = 2 : x-1
    for j = 2 : y-1
        if (close_bw2(i-1,j) == close_bw2(i+1,j)) || (close_bw2(i,j+1) == close_bw2(i,j-1))
            tepi_img2(i,j) = 0;
        else
            tepi_img2(i,j) = 1;
        end
    end
end

figure,
    subplot(2,6,1),imshow(a_gimg1),title('aritmatik'),
    subplot(2,6,2),imshow(he_gimg1),title('histogram ekualisasi'),
    subplot(2,6,3),imshow(uint8(b1)),title('konvolusi'),
    subplot(2,6,4),imshow(bw_img1),title('biner'),
    subplot(2,6,5),imshow(close_bw1),title('closing'),
    subplot(2,6,6),imshow(tepi_img1),title('4 connectivity'),
    
    subplot(2,6,7),imshow(a_gimg2),title('aritmatik'),
    subplot(2,6,8),imshow(he_gimg2),title('histogram ekualisasi'),
    subplot(2,6,9),imshow(uint8(b2)),title('konvolusi'),
    subplot(2,6,10),imshow(bw_img2),title('biner'),
    subplot(2,6,11),imshow(close_bw2),title('closing'),
    subplot(2,6,12),imshow(tepi_img2),title('4 connectivity');

